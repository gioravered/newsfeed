package com.gigi.newsfeed.ui.article

import android.app.Activity
import android.net.http.SslError
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import android.webkit.*
import androidx.appcompat.app.AppCompatActivity
import com.gigi.newsfeed.R
import kotlinx.android.synthetic.main.activity_article.*


class ArticleActivity: AppCompatActivity() {

    companion object  {
        const val articleTitleExtra: String = "articleTitle"
        const val articleUrlExtra: String = "articleUrl"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_article)
        articleWebView.webViewClient = ArticleWebViewClient()
    }

    override fun onStart() {
        super.onStart()
        setupUi()
        loadArticle()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                setResult(Activity.RESULT_OK)
                onBackPressed()
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }

    private fun setupUi() {
        val title = intent.getStringExtra(articleTitleExtra)
        supportActionBar?.title = title
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)

        articleSwipeRefresh.setOnRefreshListener {
            articleSwipeRefresh.isRefreshing = false
            loadArticle()
        }
    }

    private fun loadArticle() {
        errorText.visibility = View.INVISIBLE
        articleWebView.visibility = View.INVISIBLE
        webViewLoading.visibility = View.VISIBLE
        val url = intent.getStringExtra(articleUrlExtra)
        articleWebView.loadUrl(url)
    }

    private fun showError()  {
        errorText.visibility = View.VISIBLE
        articleWebView.visibility = View.INVISIBLE
        webViewLoading.visibility = View.INVISIBLE
    }

    inner class ArticleWebViewClient : WebViewClient() {

        override fun onReceivedError(view: WebView, request: WebResourceRequest, error: WebResourceError) {
           showError()
        }

        override fun onReceivedSslError(view: WebView?, handler: SslErrorHandler?, error: SslError?) {
            showError()
        }

        override fun onPageFinished(view: WebView?, url: String?) {
            super.onPageFinished(view, url)
            articleWebView.visibility = View.VISIBLE
            errorText.visibility = View.INVISIBLE
            webViewLoading.visibility = View.INVISIBLE
        }
    }

}