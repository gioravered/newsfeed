package com.gigi.newsfeed.ui.news_feed

import androidx.lifecycle.ViewModel

import androidx.lifecycle.ViewModelProvider
import com.gigi.newsfeed.repositories.NewsRepository


class NewsFeedViewModelFactory(private val newsRepository: NewsRepository): ViewModelProvider.Factory {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return NewsFeedViewModel(newsRepository) as T
    }
}