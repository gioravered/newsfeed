package com.gigi.newsfeed.ui.news_providers

import androidx.lifecycle.ViewModel

import androidx.lifecycle.ViewModelProvider
import com.gigi.newsfeed.repositories.NewsRepository


class NewsProvidersModelFactory(private val newsRepository: NewsRepository): ViewModelProvider.Factory {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return NewsProvidersViewModel(newsRepository) as T
    }
}