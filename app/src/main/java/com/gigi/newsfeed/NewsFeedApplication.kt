package com.gigi.newsfeed

import android.app.Application
import android.content.Context

class NewsFeedApplication : Application() {

    companion object {
        var appContext: Context? = null
    }

    override fun onCreate() {
        super.onCreate()
        appContext = applicationContext
    }
}