package com.gigi.newsfeed.repositories

import com.gigi.newsfeed.entities.Article
import com.gigi.newsfeed.entities.NewsProvider

interface NewsRepository {

    fun fetchSources(onReady: (articles: ArrayList<NewsProvider>) -> Unit)
    fun fetchHeadlines(source: String? = null, page: Int? = null, onReady: (articles: ArrayList<Article>) -> Unit)
}