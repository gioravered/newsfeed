package com.gigi.newsfeed.entities

import java.util.*

data class Article(
    val source: String,
    val author: String,
    val title: String,
    val description: String,
    val url: String,
    val imageUrl: String,
    val publishDate: Date
)