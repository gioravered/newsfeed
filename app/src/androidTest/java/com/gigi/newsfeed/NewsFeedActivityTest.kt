package com.gigi.newsfeed

import android.provider.ContactsContract.Directory.PACKAGE_NAME
import android.view.View
import androidx.recyclerview.widget.RecyclerView
import androidx.test.core.app.ActivityScenario
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.IdlingRegistry
import androidx.test.espresso.IdlingResource
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.action.ViewActions.swipeDown
import androidx.test.espresso.contrib.RecyclerViewActions
import androidx.test.espresso.intent.Intents.intended
import androidx.test.espresso.intent.matcher.IntentMatchers.*
import androidx.test.espresso.intent.rule.IntentsTestRule
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.internal.runner.junit4.AndroidJUnit4ClassRunner
import com.gigi.newsfeed.ui.article.ArticleActivity
import com.gigi.newsfeed.ui.news_feed.NewsFeedActivity
import com.gigi.newsfeed.ui.news_feed.NewsFeedAdapter
import org.hamcrest.core.AllOf.allOf
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith


@RunWith(AndroidJUnit4ClassRunner::class)
class NewsFeedActivityTest {

    @Rule @JvmField
    var mActivityRule: IntentsTestRule<NewsFeedActivity> = IntentsTestRule(NewsFeedActivity::class.java)
    private var mIdlingResource: IdlingResource? = null

    @Before
    fun registerIdlingResource() {
        val activityScenario: ActivityScenario<NewsFeedActivity> = ActivityScenario.launch(NewsFeedActivity::class.java)
        activityScenario.onActivity { activity ->
            mIdlingResource = activity.idlingResource
            IdlingRegistry.getInstance().register(mIdlingResource)
        }
    }

    @Test
    fun testSwipeRefresh() {
        onView(withId(R.id.newsFeedSwipeRefresh))
            .perform(swipeDown())
        assert(mIdlingResource?.isIdleNow?.not() ?: false)

    }

    @Test
    fun openArticleTest() {
        val recyclerView = mActivityRule.activity.findViewById<View>(R.id.newsFeedRecyclerView) as RecyclerView
        if (recyclerView.adapter!!.itemCount > 0) {
            onView(withId(R.id.newsFeedRecyclerView))
                .perform(RecyclerViewActions.actionOnItemAtPosition<NewsFeedAdapter.NewsFeedViewHolder>(0, click()))

            val adapter = recyclerView.adapter as? NewsFeedAdapter

            intended(allOf(
                toPackage(PACKAGE_NAME),
                hasExtra("articleTitle", adapter?.getItemAtPosition(0)?.title),
                hasExtra("articleUrl", adapter?.getItemAtPosition(0)?.url),
                hasComponent(ArticleActivity::class.java.name)))
        }
    }

    @After
    fun unregisterIdlingResource() {
        if (mIdlingResource != null) {
            IdlingRegistry.getInstance().unregister(mIdlingResource)
        }
    }
}