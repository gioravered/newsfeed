package com.gigi.newsfeed.entities

data class NewsProvider(
    val id: String,
    val name: String,
    val category: String
)