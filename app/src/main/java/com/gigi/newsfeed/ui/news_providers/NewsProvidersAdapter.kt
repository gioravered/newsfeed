package com.gigi.newsfeed.ui.news_providers

import android.graphics.drawable.Drawable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.ProgressBar
import androidx.appcompat.widget.AppCompatImageView
import androidx.appcompat.widget.AppCompatTextView
import androidx.constraintlayout.widget.ConstraintLayout

import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.RequestOptions
import com.gigi.newsfeed.R
import com.gigi.newsfeed.entities.Article
import com.gigi.newsfeed.entities.NewsProvider
import com.google.android.material.card.MaterialCardView
import java.lang.invoke.ConstantCallSite
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList


class NewsProvidersAdapter(
    private var selectedProviderId: String? = null,
    private val listener: NewsProviderSelectionListener): RecyclerView.Adapter<NewsProvidersAdapter.NewsProvidersViewHolder>() {

    interface NewsProviderSelectionListener {
        fun onProviderSelected(provider: NewsProvider)
    }

    private var providersData = ArrayList<NewsProvider>()
    private var selectedProvider: NewsProvider? = null

    class NewsProvidersViewHolder(view: View): RecyclerView.ViewHolder(view)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): NewsProvidersViewHolder {
        val itemView = LayoutInflater.from(parent.context).inflate(R.layout.news_provider_list_entry, parent, false)
        return NewsProvidersViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: NewsProvidersViewHolder, position: Int) {
        val provider = providersData[position]
        val providerEntryLayout = holder.itemView.findViewById<ConstraintLayout>(R.id.providerLayout)
        val providerName = holder.itemView.findViewById<AppCompatTextView>(R.id.providerName)
        val providerCategory = holder.itemView.findViewById<AppCompatTextView>(R.id.providerCategory)
        val checkmarkIcon = holder.itemView.findViewById<ImageView>(R.id.checkmarkIcon)

        providerName.text = provider.name
        providerCategory.text = provider.category
        checkmarkIcon.visibility = if (provider == selectedProvider || provider.id  == selectedProviderId) View.VISIBLE else View.INVISIBLE

        providerEntryLayout.setOnClickListener {
            selectedProvider = provider
            listener.onProviderSelected(provider)
            notifyDataSetChanged()
        }
    }

    override fun getItemCount(): Int {
        return providersData.size
    }

    fun setProviders(providers: ArrayList<NewsProvider>) {
        providersData = providers
        notifyDataSetChanged()
    }
}