package com.gigi.newsfeed.ui.news_feed

import androidx.lifecycle.ViewModel
import androidx.lifecycle.MutableLiveData
import com.gigi.newsfeed.entities.Article
import com.gigi.newsfeed.repositories.NewsRepository

class NewsFeedViewModel(private val newsRepository: NewsRepository): ViewModel() {

    val newsFeed = MutableLiveData<ArrayList<Article>>()
    private var newsPage: Int = 1

    fun refreshNews(source: String? = null) {
        this.newsRepository.fetchHeadlines(source, onReady = {
            articles -> newsFeed.value = articles
        })
    }

    fun fetchMore(source: String? = null) {
        newsPage++
        this.newsRepository.fetchHeadlines(source, newsPage, onReady = {
                articles ->
                    val all = newsFeed.value ?: ArrayList()
                    all.addAll(articles)
                    newsFeed.value = all
        })
    }
}