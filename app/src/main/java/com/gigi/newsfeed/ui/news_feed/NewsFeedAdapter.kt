package com.gigi.newsfeed.ui.news_feed

import android.graphics.drawable.Drawable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import androidx.appcompat.widget.AppCompatImageView
import androidx.appcompat.widget.AppCompatTextView

import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.RequestOptions
import com.gigi.newsfeed.R
import com.gigi.newsfeed.entities.Article
import com.google.android.material.card.MaterialCardView
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList


class NewsFeedAdapter(private val listener: ArticleClickListener): RecyclerView.Adapter<NewsFeedAdapter.NewsFeedViewHolder>() {

    interface ArticleClickListener {
        fun onArticleClick(article: Article)
    }

    private var feedData = ArrayList<Article>()

    class NewsFeedViewHolder(view: View): RecyclerView.ViewHolder(view)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): NewsFeedViewHolder {
        val itemView = LayoutInflater.from(parent.context).inflate(R.layout.article_feed_entry, parent, false)
        return NewsFeedViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: NewsFeedViewHolder, position: Int) {
        val article = feedData[position]
        val cardLayout = holder.itemView.findViewById<MaterialCardView>(R.id.feedCardLayout)
        val articleImage = holder.itemView.findViewById<AppCompatImageView>(R.id.articleImage)
        val articleTitle = holder.itemView.findViewById<AppCompatTextView>(R.id.articleTitle)
        val articleDate = holder.itemView.findViewById<AppCompatTextView>(R.id.articleDate)
        val articleSource = holder.itemView.findViewById<AppCompatTextView>(R.id.articleSource)
        val articleDescription = holder.itemView.findViewById<AppCompatTextView>(R.id.articleDescription)
        val imageLoading = holder.itemView.findViewById<ProgressBar>(R.id.imageLoading)

        articleTitle.text = article.title
        articleDate.text = article.publishDate.toString()
        articleSource.text = article.source
        articleDescription.text = article.description

        val dateFormat = SimpleDateFormat("EEE, dd MMM yyyy hh:mm", Locale.US)
        articleDate.text = dateFormat.format(article.publishDate)
        cardLayout.setOnClickListener {
            listener.onArticleClick(article)
        }

        imageLoading.visibility = View.VISIBLE
        articleImage.visibility = View.INVISIBLE
        Glide.with(holder.itemView)
            .load(article.imageUrl)
            .listener(object : RequestListener<Drawable> {
                override fun onLoadFailed(e: GlideException?, model: Any?, target: com.bumptech.glide.request.target.Target<Drawable>? , isFirstResource: Boolean): Boolean {
                    imageLoading.visibility = View.INVISIBLE
                    articleImage.visibility = View.VISIBLE
                    return false
                }
                override fun onResourceReady(p0: Drawable?, model: Any?, target: com.bumptech.glide.request.target.Target<Drawable>?, p3: DataSource?, p4: Boolean): Boolean {
                    articleImage.visibility = View.VISIBLE
                    return false
                }
            })
            .diskCacheStrategy(DiskCacheStrategy.NONE)
            .skipMemoryCache(true)
            .apply(RequestOptions().placeholder(R.drawable.image_placeholder).fallback(R.drawable.image_placeholder))
            .into(articleImage)
    }

    override fun getItemCount(): Int {
        return feedData.size
    }


    fun setArticles(articles: ArrayList<Article>) {
        feedData = articles
        notifyDataSetChanged()
    }

    fun getItemAtPosition(position: Int): Article? {
        return feedData.getOrNull(position)
    }
}