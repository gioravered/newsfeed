package com.gigi.newsfeed.ui.news_providers

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.gigi.newsfeed.R
import com.gigi.newsfeed.entities.NewsProvider
import com.gigi.newsfeed.repositories.NewsApiRepository
import kotlinx.android.synthetic.main.activity_news_providers.*
import androidx.recyclerview.widget.DividerItemDecoration



class NewsProvidersActivity: AppCompatActivity() {

    companion object {
        const val providerIdExtra = "providerId"
    }

    private var providerAdapter: NewsProvidersAdapter? = null
    private var selectedProvider: NewsProvider? = null
    private val viewModel by lazy {
        ViewModelProviders.of(
            this,
            NewsProvidersModelFactory(NewsApiRepository())).get(NewsProvidersViewModel::class.java)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_news_providers)
        viewModel.newsProviders.observe(this, Observer<ArrayList<NewsProvider>> {
                data -> updateProviders(data)
        })
        setupUi()
        loadProviders()
    }

    private fun loadProviders() {
        newsProvidersLoading.visibility = View.VISIBLE
        newsProvidersRecyclerView.visibility = View.INVISIBLE
        viewModel.fetchNewsProviders()
    }

    private fun updateProviders(providers: ArrayList<NewsProvider>) {
        providerAdapter?.setProviders(providers)
        newsProvidersLoading.visibility = View.INVISIBLE
        newsProvidersRecyclerView.visibility = View.VISIBLE
    }

    private fun setupUi() {
        setupProvidersList()
        setupOkButton()
    }

    private fun setupProvidersList() {
        val providerId = intent.getStringExtra(providerIdExtra)
        providerAdapter = NewsProvidersAdapter(providerId, object : NewsProvidersAdapter.NewsProviderSelectionListener {
            override fun onProviderSelected(provider: NewsProvider) {
                selectedProvider = provider
            }
        })
        newsProvidersRecyclerView.adapter = providerAdapter
        newsProvidersRecyclerView.layoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        val dividerItemDecoration = DividerItemDecoration(newsProvidersRecyclerView.context, LinearLayoutManager.VERTICAL)
        newsProvidersRecyclerView.addItemDecoration(dividerItemDecoration)
    }

    private fun setupOkButton() {
        okButton.setOnClickListener {
            val intent = Intent()
            intent.putExtra(providerIdExtra, selectedProvider?.id)
            setResult(Activity.RESULT_OK, intent)
            finish()
        }
    }
}