package com.gigi.newsfeed.ui.news_providers

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.gigi.newsfeed.entities.NewsProvider
import com.gigi.newsfeed.repositories.NewsRepository


class NewsProvidersViewModel(private val newsRepository: NewsRepository): ViewModel() {

    val newsProviders = MutableLiveData<ArrayList<NewsProvider>>()

    fun fetchNewsProviders() {
        newsRepository.fetchSources(onReady = {
                providers -> newsProviders.value = providers
        })
    }

}