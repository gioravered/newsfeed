package com.gigi.newsfeed.repositories

import com.gigi.newsfeed.services.NetworkService
import com.gigi.newsfeed.entities.Article
import com.gigi.newsfeed.entities.NewsProvider
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

class NewsApiRepository: NewsRepository {

    //Using newsapi.org default API key for the purpose of the assignment
    private val apiKey: String = "0985501f88374f6abd435635d88e2ccc"

    override fun fetchSources(onReady: (articles: ArrayList<NewsProvider>) -> Unit) {
        //Using US as country for the purpose of the assignment
        val url = "https://newsapi.org/v2/sources?country=us&apiKey=".plus(apiKey)
        NetworkService.httpGet(url, object : NetworkService.NetworkRequestListener {
            override fun onSuccess(response: JSONObject) {
                onReady(parseNewsProviders(response.optJSONArray("sources")))
            }

            override fun onFailure(statusCode: Int) {
                onReady(ArrayList())
            }
        })
    }

    override fun fetchHeadlines(source: String?, page: Int?, onReady: (articles: ArrayList<Article>) -> Unit) {

        var url = "https://newsapi.org/v2/top-headlines?" +
                  "apiKey=".plus(apiKey) +
                  "&pageSize=15" +
                  "&page="+ (page ?: 1)

        url += if (source == null) "&country=us" else "&sources=".plus(source)

        NetworkService.httpGet(url, object : NetworkService.NetworkRequestListener {
            override fun onSuccess(response: JSONObject) {
                onReady(parseArticles(response.optJSONArray("articles")))
            }

            override fun onFailure(statusCode: Int) {
                onReady(ArrayList())
            }
        })
    }

    private fun parseNewsProviders(providers: JSONArray?): ArrayList<NewsProvider> {
        if (providers == null) {
            return ArrayList()
        }
        val providersList = ArrayList<NewsProvider>()
        for (i in 0 until providers.length()) {
            val provider = providers.optJSONObject(i)
            try {
                providersList.add(
                    NewsProvider(
                        id = provider.getString("id"),
                        name = provider.getString("name"),
                        category = provider.optString("category") ?: "General"
                    )
                )
            } catch (e: JSONException) {

            }

        }
        return providersList
    }

    private fun parseArticles(articles: JSONArray?): ArrayList<Article> {
        if (articles == null) {
            return ArrayList()
        }
        val articlesList = ArrayList<Article>()
        for (i in 0 until articles.length()) {
            val article = articles.optJSONObject(i)
            articlesList.add(
                Article(
                    source = article.optJSONObject("source")?.optString("name") ?: "Unknown",
                    author = article.optString("author") ?: "",
                    title = article.optString("title") ?: "",
                    description = article.optString("description") ?: "",
                    url = article.optString("url") ?: "",
                    imageUrl = article.optString("urlToImage") ?: "",
                    publishDate = parseDate(article.optString("publishedAt"))

            ))
        }
        return articlesList
    }

    private fun parseDate(date: String?): Date {
        if (date == null) {
            return Date()
        }
        val dateFormat = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:SS'Z'", Locale.US)
        var parsedDate: Date? = null
        try {
            parsedDate = dateFormat.parse(date)
        } catch (e: ParseException) {

        }
        return parsedDate ?: Date()
    }
}