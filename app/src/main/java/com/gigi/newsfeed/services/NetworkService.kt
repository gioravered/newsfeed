package com.gigi.newsfeed.services

import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.JsonObjectRequest
import com.android.volley.toolbox.Volley
import com.gigi.newsfeed.NewsFeedApplication
import org.json.JSONObject

object NetworkService {

    interface NetworkRequestListener {
        fun onSuccess(response: JSONObject)
        fun onFailure(statusCode: Int)
    }
    private val requestQueue = Volley.newRequestQueue(NewsFeedApplication.appContext)


    fun httpGet(url: String, listener: NetworkRequestListener?) {
        val request = JsonObjectRequest(
            Request.Method.GET,
            url,
            null,
            Response.Listener<JSONObject> { response ->
                listener?.onSuccess(response)
            },
            Response.ErrorListener { volleyError ->
                val code = volleyError.networkResponse?.statusCode ?: 0
                listener?.onFailure(code)
            })

        requestQueue?.add(request)
    }
}