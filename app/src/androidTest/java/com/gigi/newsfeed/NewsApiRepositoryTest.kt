package com.gigi.newsfeed

import androidx.test.internal.runner.junit4.AndroidJUnit4ClassRunner
import com.gigi.newsfeed.entities.Article
import com.gigi.newsfeed.repositories.NewsApiRepository
import junit.framework.TestCase.fail
import org.junit.Test
import org.junit.runner.RunWith


@RunWith(AndroidJUnit4ClassRunner::class)
class NewsApiRepositoryTest {


    @Test
    fun fetch20HeadlinesTest() {
        var data: ArrayList<Article>? = null
        val repository = NewsApiRepository()
        repository.fetchHeadlines(ArrayList(),onReady = {
            articles -> data = articles
        })

        Thread.sleep(4000)
        if (data == null || data?.size == 0) {
            fail()
        }
    }
}