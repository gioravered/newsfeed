package com.gigi.newsfeed.ui.news_feed

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.gigi.newsfeed.R
import com.gigi.newsfeed.entities.Article
import com.gigi.newsfeed.repositories.NewsApiRepository
import com.gigi.newsfeed.ui.article.ArticleActivity
import com.gigi.newsfeed.ui.news_feed.test.NewsFeedIdlingResource
import com.gigi.newsfeed.ui.news_providers.NewsProvidersActivity
import kotlinx.android.synthetic.main.activity_news_feed.*

class NewsFeedActivity : AppCompatActivity() {

    val idlingResource: NewsFeedIdlingResource by lazy {
        NewsFeedIdlingResource()
    }

    private val articleActivityRequestCode: Int = 12345
    private val providersActivityRequestCode: Int = 12346
    private var backFromArticle: Boolean = false
    private var isFetchingNews: Boolean = false
    private var newsProviderId: String? = null
    private var feedAdapter: NewsFeedAdapter? = null
    private val viewModel by lazy {
        ViewModelProviders.of(
            this,
            NewsFeedViewModelFactory(NewsApiRepository())).get(NewsFeedViewModel::class.java)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_news_feed)
        setupUi()
        setupViewModel()
        newsProviderId = getPreferences(Context.MODE_PRIVATE).getString("newsProviderId", null)
    }

    override fun onStart() {
        super.onStart()
        if (!backFromArticle) {
            refreshFeed()
        }
        backFromArticle = false
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when(requestCode) {
            providersActivityRequestCode -> {
                if (resultCode == Activity.RESULT_OK) {
                    handleProviderChange(data?.getStringExtra(NewsProvidersActivity.providerIdExtra))
                }
            }
            articleActivityRequestCode -> {
                backFromArticle = true
            }
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.activity_news_feed_menu, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == R.id.newsProvidersButton) {
            handleNewsProviderRequest()
        }
        return super.onOptionsItemSelected(item)
    }

    private fun setupUi() {
        feedAdapter = NewsFeedAdapter(object : NewsFeedAdapter.ArticleClickListener {
            override fun onArticleClick(article: Article) {
                handleArticleClick(article = article)
            }
        })
        newsFeedRecyclerView.adapter = feedAdapter
        newsFeedRecyclerView.layoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        newsFeedRecyclerView.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                super.onScrollStateChanged(recyclerView, newState)
                if (!recyclerView.canScrollVertically(1) && !isFetchingNews) {
                    isFetchingNews = true
                    lazyLoadProgressBar.visibility = View.VISIBLE
                    viewModel.fetchMore(newsProviderId)
                }

            }
        })
        newsFeedSwipeRefresh.setOnRefreshListener {
            newsFeedSwipeRefresh.isRefreshing = false
            refreshFeed()
        }
    }

    private fun setupViewModel() {
        viewModel.newsFeed.observe(this, Observer<ArrayList<Article>> {
                data -> updateFeed(data)
        })
    }

    private fun refreshFeed() {
        newsFeedRecyclerView.visibility = View.INVISIBLE
        feedProgressBar.visibility = View.VISIBLE
        idlingResource.setIdleState(false)
        viewModel.refreshNews(newsProviderId)
    }

    private fun updateFeed(data: ArrayList<Article>) {
        feedAdapter?.setArticles(data)
        feedProgressBar.visibility = View.INVISIBLE
        lazyLoadProgressBar.visibility = View.GONE
        newsFeedRecyclerView.visibility = View.VISIBLE
        idlingResource.setIdleState(true)
        isFetchingNews = false
    }

    private fun handleArticleClick(article: Article) {
        val intent = Intent(this, ArticleActivity::class.java)
        intent.putExtra(ArticleActivity.articleTitleExtra, article.title)
        intent.putExtra(ArticleActivity.articleUrlExtra, article.url)
        startActivityForResult(intent, articleActivityRequestCode)
    }

    private fun handleNewsProviderRequest() {
        val sharedPref = getPreferences(Context.MODE_PRIVATE)
        val providerId = sharedPref.getString("newsProviderId", null)
        val intent = Intent(this, NewsProvidersActivity::class.java)
        intent.putExtra(NewsProvidersActivity.providerIdExtra, providerId)
        startActivityForResult(intent, providersActivityRequestCode)
    }

    private fun handleProviderChange(providerId: String?) {
        if (providerId == null || providerId == newsProviderId) {
            return
        }
        val sharedPref = getPreferences(Context.MODE_PRIVATE)
        sharedPref.edit().putString("newsProviderId", providerId).apply()
        newsProviderId = providerId
        refreshFeed()
    }
}
